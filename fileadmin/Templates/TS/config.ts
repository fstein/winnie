config {
	spamProtectEmailAddresses = 2
	spamProtectEmailAddresses_atSubst = <span>&#064;</span>

    language = de
   	locale_all = de_DE
    htmlTag_langKey = de

	no_cache = 0
	cache = 1

	prefixLocalAnchors = all
	simulateStaticDocuments = 0
	baseURL = http://localhost/ln-network/
	tx_realurl_enable = 1
}
