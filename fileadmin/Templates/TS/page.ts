


menu = HMENU
menu{
  #wrap = <ul>|</ul>
  entryLevel = 0
  1 = TMENU
  1{
    NO = 1
    NO.stdWrap2.wrap = <li>|</li> 
  
    CUR = 1
    CUR < .NO
    CUR.stdWrap2.wrap = <li class="active">|</li> 
  }
}

subMenu = HMENU
subMenu{
  entryLevel = 1
  
  1 = TMENU
  1{
   NO = 1
   NO.ATagParams = class="list-group-item" 
    
   CUR = 1
   CUR < .NO
   CUR.ATagParams = class="list-group-item active" 
    
  }
}

metaMenu = HMENU
metaMenu{
 special = directory
 special.value = 22
  wrap = <ul class="metamenu">|</ul>
  
  1 = TMENU
  1{
   NO = 1
   NO.stdWrap2.wrap = <li>|</li> 
    
   CUR = 1
   CUR < .NO
   #CUR.ATagParams = class="active" 
    
  }
}
temp.copyright = TEXT
temp.copyright {
   data = date:U
   strftime = %Y
   noTrimWrap = |© GJ ||
}

mainTemplate = TEMPLATE
mainTemplate{
  
 template = FILE
 template.file = fileadmin/Templates/HTML/start.html
 workOnSubpart = BODY
 marks.CONTENT < styles.content.get 
 marks.COPYRIGHT < temp.copyright
 marks.SUBMENU < subMenu
 marks.METAMENU < metaMenu
 marks.MENU < menu
 
}
subpageTemplate = TEMPLATE
subpageTemplate{
  template = FILE
  template.file = fileadmin/Templates/HTML/about.html
  workOnSubpart = BODY
  marks.CONTENT < styles.content.get
  marks.MENU < menu
  marks.METAMENU < metaMenu
  marks.SUBMENU < subMenu
  marks.COPYRIGHT < temp.copyright
}


page = PAGE
page.10 < subpageTemplate
page.includeCSS{
  
   file2 = fileadmin/Templates/CSS/bootstrap.min.css
   file3 = fileadmin/Templates/CSS/offcanvas.css
   file4= fileadmin/Templates/CSS/winnie.css
}#ElementinformationenAttributePositionAndereElternelementeKindelemente
 page.headerData{
 }

page.includeJS{
  file19= fileadmin/Templates/JS/jquery.min.js
  file20= fileadmin/Templates/JS/bootstrap.min.js
  file21= fileadmin/Templates/JS/offcanvas.js
}


[globalVar = TSFE:id = 1]
page.10 >
page.10 < mainTemplate
}
page.includeJS{
 
}
[global]

