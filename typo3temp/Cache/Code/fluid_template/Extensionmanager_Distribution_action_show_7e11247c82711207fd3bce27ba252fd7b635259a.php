<?php
class FluidCache_Extensionmanager_Distribution_action_show_7e11247c82711207fd3bce27ba252fd7b635259a extends \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\CMS\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {

return 'main';
}
public function hasLayout() {
return TRUE;
}

/**
 * section docheader-buttons
 */
public function section_82416aa889dc891ac3382685ebae30417e96849a(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '
	';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments1 = array();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments2 = array();
$arguments2['controller'] = 'List';
$arguments2['action'] = 'distributions';
$arguments2['arguments'] = array (
);
$arguments2['extensionName'] = NULL;
$arguments2['pluginName'] = NULL;
$arguments2['pageUid'] = NULL;
$arguments2['pageType'] = 0;
$arguments2['noCache'] = false;
$arguments2['noCacheHash'] = false;
$arguments2['section'] = '';
$arguments2['format'] = '';
$arguments2['linkAccessRestrictedPages'] = false;
$arguments2['additionalParams'] = array (
);
$arguments2['absolute'] = false;
$arguments2['addQueryString'] = false;
$arguments2['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments2['addQueryStringMethod'] = NULL;
$renderChildrenClosure3 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper4 = $self->getViewHelper('$viewHelper4', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper4->setArguments($arguments2);
$viewHelper4->setRenderingContext($renderingContext);
$viewHelper4->setRenderChildrenClosure($renderChildrenClosure3);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments1['uri'] = $viewHelper4->initializeArgumentsAndRender();
$arguments1['icon'] = 'actions-view-go-back';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments5 = array();
$arguments5['key'] = 'extConfTemplate.backToList';
$arguments5['id'] = NULL;
$arguments5['default'] = NULL;
$arguments5['htmlEscape'] = NULL;
$arguments5['arguments'] = NULL;
$arguments5['extensionName'] = NULL;
$renderChildrenClosure6 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper7 = $self->getViewHelper('$viewHelper7', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper7->setArguments($arguments5);
$viewHelper7->setRenderingContext($renderingContext);
$viewHelper7->setRenderChildrenClosure($renderChildrenClosure6);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments1['title'] = $viewHelper7->initializeArgumentsAndRender();
$arguments1['additionalAttributes'] = NULL;
$renderChildrenClosure8 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper9 = $self->getViewHelper('$viewHelper9', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper9->setArguments($arguments1);
$viewHelper9->setRenderingContext($renderingContext);
$viewHelper9->setRenderChildrenClosure($renderChildrenClosure8);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output0 .= $viewHelper9->initializeArgumentsAndRender();

$output0 .= '
';

return $output0;
}
/**
 * section module-headline
 */
public function section_448d1ad99edd62d80682fc5d4e038788bb925e4c(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;

return '
';
}
/**
 * section Content
 */
public function section_4f9be057f0ea5d2ba72fd2c810e8d7b9aa98b469(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output10 = '';

$output10 .= '
	';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\FlashMessagesViewHelper
$arguments11 = array();
$arguments11['class'] = 'distribution-detail-flashmessage';
$arguments11['renderMode'] = 'div';
$arguments11['additionalAttributes'] = NULL;
$arguments11['dir'] = NULL;
$arguments11['id'] = NULL;
$arguments11['lang'] = NULL;
$arguments11['style'] = NULL;
$arguments11['title'] = NULL;
$arguments11['accesskey'] = NULL;
$arguments11['tabindex'] = NULL;
$arguments11['onclick'] = NULL;
$renderChildrenClosure12 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper13 = $self->getViewHelper('$viewHelper13', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\FlashMessagesViewHelper');
$viewHelper13->setArguments($arguments11);
$viewHelper13->setRenderingContext($renderingContext);
$viewHelper13->setRenderChildrenClosure($renderChildrenClosure12);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\FlashMessagesViewHelper

$output10 .= $viewHelper13->initializeArgumentsAndRender();

$output10 .= '
	<div class="distribution-detail">
		<div class="distribution-detail-previewpane">
			';
// Rendering ViewHelper TYPO3\CMS\Extensionmanager\ViewHelpers\ImageViewHelper
$arguments14 = array();
$output15 = '';

$output15 .= 'EXT:';

$output15 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.extensionKey', $renderingContext);

$output15 .= '/Resources/Public/Images/DistributionWelcome.png';
$arguments14['src'] = $output15;
$arguments14['alt'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'distribution.title', $renderingContext);
$arguments14['class'] = 'distribution-detail-preview';
$arguments14['additionalAttributes'] = NULL;
$arguments14['width'] = NULL;
$arguments14['height'] = NULL;
$arguments14['minWidth'] = NULL;
$arguments14['minHeight'] = NULL;
$arguments14['maxWidth'] = NULL;
$arguments14['maxHeight'] = NULL;
$arguments14['fallbackImage'] = '';
$arguments14['dir'] = NULL;
$arguments14['id'] = NULL;
$arguments14['lang'] = NULL;
$arguments14['style'] = NULL;
$arguments14['title'] = NULL;
$arguments14['accesskey'] = NULL;
$arguments14['tabindex'] = NULL;
$arguments14['onclick'] = NULL;
$arguments14['ismap'] = NULL;
$arguments14['longdesc'] = NULL;
$arguments14['usemap'] = NULL;
$renderChildrenClosure16 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper17 = $self->getViewHelper('$viewHelper17', $renderingContext, 'TYPO3\CMS\Extensionmanager\ViewHelpers\ImageViewHelper');
$viewHelper17->setArguments($arguments14);
$viewHelper17->setRenderingContext($renderingContext);
$viewHelper17->setRenderChildrenClosure($renderChildrenClosure16);
// End of ViewHelper TYPO3\CMS\Extensionmanager\ViewHelpers\ImageViewHelper

$output10 .= $viewHelper17->initializeArgumentsAndRender();

$output10 .= '
		</div>
		<div class="distribution-detail-body">
			<div class="distribution-detail-header">
				<h1>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments18 = array();
$arguments18['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.title', $renderingContext);
$arguments18['keepQuotes'] = false;
$arguments18['encoding'] = NULL;
$arguments18['doubleEncode'] = true;
$renderChildrenClosure19 = function() use ($renderingContext, $self) {
return NULL;
};
$value20 = ($arguments18['value'] !== NULL ? $arguments18['value'] : $renderChildrenClosure19());

$output10 .= (!is_string($value20) ? $value20 : htmlspecialchars($value20, ($arguments18['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments18['encoding'] !== NULL ? $arguments18['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments18['doubleEncode']));

$output10 .= '</h1>
				<p>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments21 = array();
$arguments21['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.description', $renderingContext);
$arguments21['keepQuotes'] = false;
$arguments21['encoding'] = NULL;
$arguments21['doubleEncode'] = true;
$renderChildrenClosure22 = function() use ($renderingContext, $self) {
return NULL;
};
$value23 = ($arguments21['value'] !== NULL ? $arguments21['value'] : $renderChildrenClosure22());

$output10 .= (!is_string($value23) ? $value23 : htmlspecialchars($value23, ($arguments21['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments21['encoding'] !== NULL ? $arguments21['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments21['doubleEncode']));

$output10 .= '</p>
			</div>
			<ul class="list-unstyled">
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments24 = array();
// Rendering Boolean node
$arguments24['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'distributionActive', $renderingContext));
$arguments24['then'] = NULL;
$arguments24['else'] = NULL;
$renderChildrenClosure25 = function() use ($renderingContext, $self) {
$output26 = '';

$output26 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments27 = array();
$renderChildrenClosure28 = function() use ($renderingContext, $self) {
$output29 = '';

$output29 .= '
						<li>
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments30 = array();
$arguments30['action'] = 'installDistribution';
$arguments30['controller'] = 'Download';
// Rendering Array
$array31 = array();
$array31['extension'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension', $renderingContext);
$arguments30['arguments'] = $array31;
$arguments30['class'] = 't3-button t3-button-action-installdistribution';
$arguments30['additionalAttributes'] = NULL;
$arguments30['extensionName'] = NULL;
$arguments30['pluginName'] = NULL;
$arguments30['pageUid'] = NULL;
$arguments30['pageType'] = 0;
$arguments30['noCache'] = false;
$arguments30['noCacheHash'] = false;
$arguments30['section'] = '';
$arguments30['format'] = '';
$arguments30['linkAccessRestrictedPages'] = false;
$arguments30['additionalParams'] = array (
);
$arguments30['absolute'] = false;
$arguments30['addQueryString'] = false;
$arguments30['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments30['addQueryStringMethod'] = NULL;
$arguments30['dir'] = NULL;
$arguments30['id'] = NULL;
$arguments30['lang'] = NULL;
$arguments30['style'] = NULL;
$arguments30['title'] = NULL;
$arguments30['accesskey'] = NULL;
$arguments30['tabindex'] = NULL;
$arguments30['onclick'] = NULL;
$arguments30['name'] = NULL;
$arguments30['rel'] = NULL;
$arguments30['rev'] = NULL;
$arguments30['target'] = NULL;
$renderChildrenClosure32 = function() use ($renderingContext, $self) {
$output33 = '';

$output33 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments34 = array();
$arguments34['icon'] = 'actions-system-extension-import';
$arguments34['uri'] = '';
$arguments34['title'] = '';
$arguments34['additionalAttributes'] = NULL;
$renderChildrenClosure35 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper36 = $self->getViewHelper('$viewHelper36', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper36->setArguments($arguments34);
$viewHelper36->setRenderingContext($renderingContext);
$viewHelper36->setRenderChildrenClosure($renderChildrenClosure35);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output33 .= $viewHelper36->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments37 = array();
$arguments37['key'] = 'extensionList.installDistribution';
$arguments37['id'] = NULL;
$arguments37['default'] = NULL;
$arguments37['htmlEscape'] = NULL;
$arguments37['arguments'] = NULL;
$arguments37['extensionName'] = NULL;
$renderChildrenClosure38 = function() use ($renderingContext, $self) {
return 'Install';
};
$viewHelper39 = $self->getViewHelper('$viewHelper39', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper39->setArguments($arguments37);
$viewHelper39->setRenderingContext($renderingContext);
$viewHelper39->setRenderChildrenClosure($renderChildrenClosure38);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output33 .= $viewHelper39->initializeArgumentsAndRender();

$output33 .= '
							';
return $output33;
};
$viewHelper40 = $self->getViewHelper('$viewHelper40', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper40->setArguments($arguments30);
$viewHelper40->setRenderingContext($renderingContext);
$viewHelper40->setRenderChildrenClosure($renderChildrenClosure32);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Link\ActionViewHelper

$output29 .= $viewHelper40->initializeArgumentsAndRender();

$output29 .= '
						</li>
					';
return $output29;
};
$viewHelper41 = $self->getViewHelper('$viewHelper41', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper41->setArguments($arguments27);
$viewHelper41->setRenderingContext($renderingContext);
$viewHelper41->setRenderChildrenClosure($renderChildrenClosure28);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output26 .= $viewHelper41->initializeArgumentsAndRender();

$output26 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments42 = array();
$renderChildrenClosure43 = function() use ($renderingContext, $self) {
$output44 = '';

$output44 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments45 = array();
// Rendering Boolean node
$arguments45['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configurationLink', $renderingContext));
$arguments45['then'] = NULL;
$arguments45['else'] = NULL;
$renderChildrenClosure46 = function() use ($renderingContext, $self) {
$output47 = '';

$output47 .= '
							<li>
								<a href="';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments48 = array();
$arguments48['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configurationLink', $renderingContext);
$arguments48['keepQuotes'] = false;
$arguments48['encoding'] = NULL;
$arguments48['doubleEncode'] = true;
$renderChildrenClosure49 = function() use ($renderingContext, $self) {
return NULL;
};
$value50 = ($arguments48['value'] !== NULL ? $arguments48['value'] : $renderChildrenClosure49());

$output47 .= (!is_string($value50) ? $value50 : htmlspecialchars($value50, ($arguments48['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments48['encoding'] !== NULL ? $arguments48['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments48['doubleEncode']));

$output47 .= '" class="distribution-openViewModule t3-button" onclick="top.goToModule(\'web_ViewpageView\');">
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments51 = array();
$arguments51['icon'] = 'actions-system-extension-configure';
$arguments51['uri'] = '';
$arguments51['title'] = '';
$arguments51['additionalAttributes'] = NULL;
$renderChildrenClosure52 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper53 = $self->getViewHelper('$viewHelper53', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper53->setArguments($arguments51);
$viewHelper53->setRenderingContext($renderingContext);
$viewHelper53->setRenderChildrenClosure($renderChildrenClosure52);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output47 .= $viewHelper53->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments54 = array();
$arguments54['key'] = 'extensionList.configure';
$arguments54['id'] = NULL;
$arguments54['default'] = NULL;
$arguments54['htmlEscape'] = NULL;
$arguments54['arguments'] = NULL;
$arguments54['extensionName'] = NULL;
$renderChildrenClosure55 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper56 = $self->getViewHelper('$viewHelper56', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper56->setArguments($arguments54);
$viewHelper56->setRenderingContext($renderingContext);
$viewHelper56->setRenderChildrenClosure($renderChildrenClosure55);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output47 .= $viewHelper56->initializeArgumentsAndRender();

$output47 .= '
								</a>
							</li>
						';
return $output47;
};
$viewHelper57 = $self->getViewHelper('$viewHelper57', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper57->setArguments($arguments45);
$viewHelper57->setRenderingContext($renderingContext);
$viewHelper57->setRenderChildrenClosure($renderChildrenClosure46);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output44 .= $viewHelper57->initializeArgumentsAndRender();

$output44 .= '
						<li>
							<button class="distribution-openViewModule t3-button" onclick="top.goToModule(\'web_ViewpageView\');">
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments58 = array();
$arguments58['icon'] = 'actions-document-view';
$arguments58['uri'] = '';
$arguments58['title'] = '';
$arguments58['additionalAttributes'] = NULL;
$renderChildrenClosure59 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper60 = $self->getViewHelper('$viewHelper60', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper60->setArguments($arguments58);
$viewHelper60->setRenderingContext($renderingContext);
$viewHelper60->setRenderChildrenClosure($renderChildrenClosure59);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output44 .= $viewHelper60->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments61 = array();
$arguments61['key'] = 'distribution.welcome.openViewModule';
$arguments61['id'] = NULL;
$arguments61['default'] = NULL;
$arguments61['htmlEscape'] = NULL;
$arguments61['arguments'] = NULL;
$arguments61['extensionName'] = NULL;
$renderChildrenClosure62 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper63 = $self->getViewHelper('$viewHelper63', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper63->setArguments($arguments61);
$viewHelper63->setRenderingContext($renderingContext);
$viewHelper63->setRenderChildrenClosure($renderChildrenClosure62);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output44 .= $viewHelper63->initializeArgumentsAndRender();

$output44 .= '
							</button>
						</li>
						<li>
							<button class="distribution-openPageModule t3-button" onclick="top.goToModule(\'web_page\');">
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments64 = array();
$arguments64['icon'] = 'actions-document-open';
$arguments64['uri'] = '';
$arguments64['title'] = '';
$arguments64['additionalAttributes'] = NULL;
$renderChildrenClosure65 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper66 = $self->getViewHelper('$viewHelper66', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper66->setArguments($arguments64);
$viewHelper66->setRenderingContext($renderingContext);
$viewHelper66->setRenderChildrenClosure($renderChildrenClosure65);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output44 .= $viewHelper66->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments67 = array();
$arguments67['key'] = 'distribution.welcome.openPageModule';
$arguments67['id'] = NULL;
$arguments67['default'] = NULL;
$arguments67['htmlEscape'] = NULL;
$arguments67['arguments'] = NULL;
$arguments67['extensionName'] = NULL;
$renderChildrenClosure68 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper69 = $self->getViewHelper('$viewHelper69', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper69->setArguments($arguments67);
$viewHelper69->setRenderingContext($renderingContext);
$viewHelper69->setRenderChildrenClosure($renderChildrenClosure68);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output44 .= $viewHelper69->initializeArgumentsAndRender();

$output44 .= '
							</button>
						</li>
					';
return $output44;
};
$viewHelper70 = $self->getViewHelper('$viewHelper70', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper70->setArguments($arguments42);
$viewHelper70->setRenderingContext($renderingContext);
$viewHelper70->setRenderChildrenClosure($renderChildrenClosure43);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output26 .= $viewHelper70->initializeArgumentsAndRender();

$output26 .= '
				';
return $output26;
};
$arguments24['__elseClosure'] = function() use ($renderingContext, $self) {
$output71 = '';

$output71 .= '
						<li>
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments72 = array();
$arguments72['action'] = 'installDistribution';
$arguments72['controller'] = 'Download';
// Rendering Array
$array73 = array();
$array73['extension'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension', $renderingContext);
$arguments72['arguments'] = $array73;
$arguments72['class'] = 't3-button t3-button-action-installdistribution';
$arguments72['additionalAttributes'] = NULL;
$arguments72['extensionName'] = NULL;
$arguments72['pluginName'] = NULL;
$arguments72['pageUid'] = NULL;
$arguments72['pageType'] = 0;
$arguments72['noCache'] = false;
$arguments72['noCacheHash'] = false;
$arguments72['section'] = '';
$arguments72['format'] = '';
$arguments72['linkAccessRestrictedPages'] = false;
$arguments72['additionalParams'] = array (
);
$arguments72['absolute'] = false;
$arguments72['addQueryString'] = false;
$arguments72['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments72['addQueryStringMethod'] = NULL;
$arguments72['dir'] = NULL;
$arguments72['id'] = NULL;
$arguments72['lang'] = NULL;
$arguments72['style'] = NULL;
$arguments72['title'] = NULL;
$arguments72['accesskey'] = NULL;
$arguments72['tabindex'] = NULL;
$arguments72['onclick'] = NULL;
$arguments72['name'] = NULL;
$arguments72['rel'] = NULL;
$arguments72['rev'] = NULL;
$arguments72['target'] = NULL;
$renderChildrenClosure74 = function() use ($renderingContext, $self) {
$output75 = '';

$output75 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments76 = array();
$arguments76['icon'] = 'actions-system-extension-import';
$arguments76['uri'] = '';
$arguments76['title'] = '';
$arguments76['additionalAttributes'] = NULL;
$renderChildrenClosure77 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper78 = $self->getViewHelper('$viewHelper78', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper78->setArguments($arguments76);
$viewHelper78->setRenderingContext($renderingContext);
$viewHelper78->setRenderChildrenClosure($renderChildrenClosure77);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output75 .= $viewHelper78->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments79 = array();
$arguments79['key'] = 'extensionList.installDistribution';
$arguments79['id'] = NULL;
$arguments79['default'] = NULL;
$arguments79['htmlEscape'] = NULL;
$arguments79['arguments'] = NULL;
$arguments79['extensionName'] = NULL;
$renderChildrenClosure80 = function() use ($renderingContext, $self) {
return 'Install';
};
$viewHelper81 = $self->getViewHelper('$viewHelper81', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper81->setArguments($arguments79);
$viewHelper81->setRenderingContext($renderingContext);
$viewHelper81->setRenderChildrenClosure($renderChildrenClosure80);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output75 .= $viewHelper81->initializeArgumentsAndRender();

$output75 .= '
							';
return $output75;
};
$viewHelper82 = $self->getViewHelper('$viewHelper82', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper82->setArguments($arguments72);
$viewHelper82->setRenderingContext($renderingContext);
$viewHelper82->setRenderChildrenClosure($renderChildrenClosure74);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Link\ActionViewHelper

$output71 .= $viewHelper82->initializeArgumentsAndRender();

$output71 .= '
						</li>
					';
return $output71;
};
$arguments24['__thenClosure'] = function() use ($renderingContext, $self) {
$output83 = '';

$output83 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments84 = array();
// Rendering Boolean node
$arguments84['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configurationLink', $renderingContext));
$arguments84['then'] = NULL;
$arguments84['else'] = NULL;
$renderChildrenClosure85 = function() use ($renderingContext, $self) {
$output86 = '';

$output86 .= '
							<li>
								<a href="';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments87 = array();
$arguments87['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configurationLink', $renderingContext);
$arguments87['keepQuotes'] = false;
$arguments87['encoding'] = NULL;
$arguments87['doubleEncode'] = true;
$renderChildrenClosure88 = function() use ($renderingContext, $self) {
return NULL;
};
$value89 = ($arguments87['value'] !== NULL ? $arguments87['value'] : $renderChildrenClosure88());

$output86 .= (!is_string($value89) ? $value89 : htmlspecialchars($value89, ($arguments87['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments87['encoding'] !== NULL ? $arguments87['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments87['doubleEncode']));

$output86 .= '" class="distribution-openViewModule t3-button" onclick="top.goToModule(\'web_ViewpageView\');">
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments90 = array();
$arguments90['icon'] = 'actions-system-extension-configure';
$arguments90['uri'] = '';
$arguments90['title'] = '';
$arguments90['additionalAttributes'] = NULL;
$renderChildrenClosure91 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper92 = $self->getViewHelper('$viewHelper92', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper92->setArguments($arguments90);
$viewHelper92->setRenderingContext($renderingContext);
$viewHelper92->setRenderChildrenClosure($renderChildrenClosure91);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output86 .= $viewHelper92->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments93 = array();
$arguments93['key'] = 'extensionList.configure';
$arguments93['id'] = NULL;
$arguments93['default'] = NULL;
$arguments93['htmlEscape'] = NULL;
$arguments93['arguments'] = NULL;
$arguments93['extensionName'] = NULL;
$renderChildrenClosure94 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper95 = $self->getViewHelper('$viewHelper95', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper95->setArguments($arguments93);
$viewHelper95->setRenderingContext($renderingContext);
$viewHelper95->setRenderChildrenClosure($renderChildrenClosure94);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output86 .= $viewHelper95->initializeArgumentsAndRender();

$output86 .= '
								</a>
							</li>
						';
return $output86;
};
$viewHelper96 = $self->getViewHelper('$viewHelper96', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper96->setArguments($arguments84);
$viewHelper96->setRenderingContext($renderingContext);
$viewHelper96->setRenderChildrenClosure($renderChildrenClosure85);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output83 .= $viewHelper96->initializeArgumentsAndRender();

$output83 .= '
						<li>
							<button class="distribution-openViewModule t3-button" onclick="top.goToModule(\'web_ViewpageView\');">
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments97 = array();
$arguments97['icon'] = 'actions-document-view';
$arguments97['uri'] = '';
$arguments97['title'] = '';
$arguments97['additionalAttributes'] = NULL;
$renderChildrenClosure98 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper99 = $self->getViewHelper('$viewHelper99', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper99->setArguments($arguments97);
$viewHelper99->setRenderingContext($renderingContext);
$viewHelper99->setRenderChildrenClosure($renderChildrenClosure98);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output83 .= $viewHelper99->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments100 = array();
$arguments100['key'] = 'distribution.welcome.openViewModule';
$arguments100['id'] = NULL;
$arguments100['default'] = NULL;
$arguments100['htmlEscape'] = NULL;
$arguments100['arguments'] = NULL;
$arguments100['extensionName'] = NULL;
$renderChildrenClosure101 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper102 = $self->getViewHelper('$viewHelper102', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper102->setArguments($arguments100);
$viewHelper102->setRenderingContext($renderingContext);
$viewHelper102->setRenderChildrenClosure($renderChildrenClosure101);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output83 .= $viewHelper102->initializeArgumentsAndRender();

$output83 .= '
							</button>
						</li>
						<li>
							<button class="distribution-openPageModule t3-button" onclick="top.goToModule(\'web_page\');">
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments103 = array();
$arguments103['icon'] = 'actions-document-open';
$arguments103['uri'] = '';
$arguments103['title'] = '';
$arguments103['additionalAttributes'] = NULL;
$renderChildrenClosure104 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper105 = $self->getViewHelper('$viewHelper105', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper105->setArguments($arguments103);
$viewHelper105->setRenderingContext($renderingContext);
$viewHelper105->setRenderChildrenClosure($renderChildrenClosure104);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output83 .= $viewHelper105->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments106 = array();
$arguments106['key'] = 'distribution.welcome.openPageModule';
$arguments106['id'] = NULL;
$arguments106['default'] = NULL;
$arguments106['htmlEscape'] = NULL;
$arguments106['arguments'] = NULL;
$arguments106['extensionName'] = NULL;
$renderChildrenClosure107 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper108 = $self->getViewHelper('$viewHelper108', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper108->setArguments($arguments106);
$viewHelper108->setRenderingContext($renderingContext);
$viewHelper108->setRenderChildrenClosure($renderChildrenClosure107);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output83 .= $viewHelper108->initializeArgumentsAndRender();

$output83 .= '
							</button>
						</li>
					';
return $output83;
};
$viewHelper109 = $self->getViewHelper('$viewHelper109', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper109->setArguments($arguments24);
$viewHelper109->setRenderingContext($renderingContext);
$viewHelper109->setRenderChildrenClosure($renderChildrenClosure25);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output10 .= $viewHelper109->initializeArgumentsAndRender();

$output10 .= '
			</ul>
			<dl class="description-horizontal description-horizontal-wide distribution-detail-summary">
				<dt>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments110 = array();
$arguments110['key'] = 'extensionList.distribution.title';
$arguments110['id'] = NULL;
$arguments110['default'] = NULL;
$arguments110['htmlEscape'] = NULL;
$arguments110['arguments'] = NULL;
$arguments110['extensionName'] = NULL;
$renderChildrenClosure111 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper112 = $self->getViewHelper('$viewHelper112', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper112->setArguments($arguments110);
$viewHelper112->setRenderingContext($renderingContext);
$viewHelper112->setRenderChildrenClosure($renderChildrenClosure111);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output10 .= $viewHelper112->initializeArgumentsAndRender();

$output10 .= '</dt>
				<dd>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments113 = array();
$arguments113['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.title', $renderingContext);
$arguments113['keepQuotes'] = false;
$arguments113['encoding'] = NULL;
$arguments113['doubleEncode'] = true;
$renderChildrenClosure114 = function() use ($renderingContext, $self) {
return NULL;
};
$value115 = ($arguments113['value'] !== NULL ? $arguments113['value'] : $renderChildrenClosure114());

$output10 .= (!is_string($value115) ? $value115 : htmlspecialchars($value115, ($arguments113['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments113['encoding'] !== NULL ? $arguments113['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments113['doubleEncode']));

$output10 .= '</dd>
				<dt>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments116 = array();
$arguments116['key'] = 'extensionList.distribution.key';
$arguments116['id'] = NULL;
$arguments116['default'] = NULL;
$arguments116['htmlEscape'] = NULL;
$arguments116['arguments'] = NULL;
$arguments116['extensionName'] = NULL;
$renderChildrenClosure117 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper118 = $self->getViewHelper('$viewHelper118', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper118->setArguments($arguments116);
$viewHelper118->setRenderingContext($renderingContext);
$viewHelper118->setRenderChildrenClosure($renderChildrenClosure117);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output10 .= $viewHelper118->initializeArgumentsAndRender();

$output10 .= '</dt>
				<dd>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments119 = array();
$arguments119['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.extensionKey', $renderingContext);
$arguments119['keepQuotes'] = false;
$arguments119['encoding'] = NULL;
$arguments119['doubleEncode'] = true;
$renderChildrenClosure120 = function() use ($renderingContext, $self) {
return NULL;
};
$value121 = ($arguments119['value'] !== NULL ? $arguments119['value'] : $renderChildrenClosure120());

$output10 .= (!is_string($value121) ? $value121 : htmlspecialchars($value121, ($arguments119['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments119['encoding'] !== NULL ? $arguments119['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments119['doubleEncode']));

$output10 .= '</dd>
				<dt>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments122 = array();
$arguments122['key'] = 'extensionList.distribution.version';
$arguments122['id'] = NULL;
$arguments122['default'] = NULL;
$arguments122['htmlEscape'] = NULL;
$arguments122['arguments'] = NULL;
$arguments122['extensionName'] = NULL;
$renderChildrenClosure123 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper124 = $self->getViewHelper('$viewHelper124', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper124->setArguments($arguments122);
$viewHelper124->setRenderingContext($renderingContext);
$viewHelper124->setRenderChildrenClosure($renderChildrenClosure123);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output10 .= $viewHelper124->initializeArgumentsAndRender();

$output10 .= '</dt>
				<dd>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments125 = array();
$arguments125['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.version', $renderingContext);
$arguments125['keepQuotes'] = false;
$arguments125['encoding'] = NULL;
$arguments125['doubleEncode'] = true;
$renderChildrenClosure126 = function() use ($renderingContext, $self) {
return NULL;
};
$value127 = ($arguments125['value'] !== NULL ? $arguments125['value'] : $renderChildrenClosure126());

$output10 .= (!is_string($value127) ? $value127 : htmlspecialchars($value127, ($arguments125['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments125['encoding'] !== NULL ? $arguments125['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments125['doubleEncode']));

$output10 .= ' (';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\DateViewHelper
$arguments128 = array();
$arguments128['format'] = 'd.m.Y';
$arguments128['date'] = NULL;
$renderChildrenClosure129 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.lastUpdated', $renderingContext);
};
$viewHelper130 = $self->getViewHelper('$viewHelper130', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\DateViewHelper');
$viewHelper130->setArguments($arguments128);
$viewHelper130->setRenderingContext($renderingContext);
$viewHelper130->setRenderChildrenClosure($renderChildrenClosure129);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\DateViewHelper

$output10 .= $viewHelper130->initializeArgumentsAndRender();

$output10 .= ')<br><span class="';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments131 = array();
$arguments131['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.stateString', $renderingContext);
$arguments131['keepQuotes'] = false;
$arguments131['encoding'] = NULL;
$arguments131['doubleEncode'] = true;
$renderChildrenClosure132 = function() use ($renderingContext, $self) {
return NULL;
};
$value133 = ($arguments131['value'] !== NULL ? $arguments131['value'] : $renderChildrenClosure132());

$output10 .= (!is_string($value133) ? $value133 : htmlspecialchars($value133, ($arguments131['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments131['encoding'] !== NULL ? $arguments131['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments131['doubleEncode']));

$output10 .= '">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments134 = array();
$arguments134['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.stateString', $renderingContext);
$arguments134['keepQuotes'] = false;
$arguments134['encoding'] = NULL;
$arguments134['doubleEncode'] = true;
$renderChildrenClosure135 = function() use ($renderingContext, $self) {
return NULL;
};
$value136 = ($arguments134['value'] !== NULL ? $arguments134['value'] : $renderChildrenClosure135());

$output10 .= (!is_string($value136) ? $value136 : htmlspecialchars($value136, ($arguments134['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments134['encoding'] !== NULL ? $arguments134['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments134['doubleEncode']));

$output10 .= '</span></dd>
				<dt>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments137 = array();
$arguments137['key'] = 'extensionList.distribution.author';
$arguments137['id'] = NULL;
$arguments137['default'] = NULL;
$arguments137['htmlEscape'] = NULL;
$arguments137['arguments'] = NULL;
$arguments137['extensionName'] = NULL;
$renderChildrenClosure138 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper139 = $self->getViewHelper('$viewHelper139', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper139->setArguments($arguments137);
$viewHelper139->setRenderingContext($renderingContext);
$viewHelper139->setRenderChildrenClosure($renderChildrenClosure138);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output10 .= $viewHelper139->initializeArgumentsAndRender();

$output10 .= '</dt>
				<dd>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments140 = array();
$arguments140['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.authorName', $renderingContext);
$arguments140['keepQuotes'] = false;
$arguments140['encoding'] = NULL;
$arguments140['doubleEncode'] = true;
$renderChildrenClosure141 = function() use ($renderingContext, $self) {
return NULL;
};
$value142 = ($arguments140['value'] !== NULL ? $arguments140['value'] : $renderChildrenClosure141());

$output10 .= (!is_string($value142) ? $value142 : htmlspecialchars($value142, ($arguments140['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments140['encoding'] !== NULL ? $arguments140['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments140['doubleEncode']));

$output10 .= '</dd>
				<dt>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments143 = array();
$arguments143['key'] = 'extensionList.distribution.downloads';
$arguments143['id'] = NULL;
$arguments143['default'] = NULL;
$arguments143['htmlEscape'] = NULL;
$arguments143['arguments'] = NULL;
$arguments143['extensionName'] = NULL;
$renderChildrenClosure144 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper145 = $self->getViewHelper('$viewHelper145', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper145->setArguments($arguments143);
$viewHelper145->setRenderingContext($renderingContext);
$viewHelper145->setRenderChildrenClosure($renderChildrenClosure144);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output10 .= $viewHelper145->initializeArgumentsAndRender();

$output10 .= '</dt>
				<dd>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments146 = array();
$arguments146['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.alldownloadcounter', $renderingContext);
$arguments146['keepQuotes'] = false;
$arguments146['encoding'] = NULL;
$arguments146['doubleEncode'] = true;
$renderChildrenClosure147 = function() use ($renderingContext, $self) {
return NULL;
};
$value148 = ($arguments146['value'] !== NULL ? $arguments146['value'] : $renderChildrenClosure147());

$output10 .= (!is_string($value148) ? $value148 : htmlspecialchars($value148, ($arguments146['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments146['encoding'] !== NULL ? $arguments146['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments146['doubleEncode']));

$output10 .= '</dd>
			</dl>
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments149 = array();
// Rendering Boolean node
$arguments149['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.dependencies', $renderingContext));
$arguments149['then'] = NULL;
$arguments149['else'] = NULL;
$renderChildrenClosure150 = function() use ($renderingContext, $self) {
$output151 = '';

$output151 .= '
				<h2>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments152 = array();
$arguments152['key'] = 'distribution.dependency.headline';
$arguments152['id'] = NULL;
$arguments152['default'] = NULL;
$arguments152['htmlEscape'] = NULL;
$arguments152['arguments'] = NULL;
$arguments152['extensionName'] = NULL;
$renderChildrenClosure153 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper154 = $self->getViewHelper('$viewHelper154', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper154->setArguments($arguments152);
$viewHelper154->setRenderingContext($renderingContext);
$viewHelper154->setRenderChildrenClosure($renderChildrenClosure153);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output151 .= $viewHelper154->initializeArgumentsAndRender();

$output151 .= '</h2>
				<table class="t3-table">
					<thead>
						<tr class="t3-row-header">
							<td>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments155 = array();
$arguments155['key'] = 'distribution.dependency.identifier';
$arguments155['id'] = NULL;
$arguments155['default'] = NULL;
$arguments155['htmlEscape'] = NULL;
$arguments155['arguments'] = NULL;
$arguments155['extensionName'] = NULL;
$renderChildrenClosure156 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper157 = $self->getViewHelper('$viewHelper157', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper157->setArguments($arguments155);
$viewHelper157->setRenderingContext($renderingContext);
$viewHelper157->setRenderChildrenClosure($renderChildrenClosure156);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output151 .= $viewHelper157->initializeArgumentsAndRender();

$output151 .= '</td>
							<td>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments158 = array();
$arguments158['key'] = 'distribution.dependency.type';
$arguments158['id'] = NULL;
$arguments158['default'] = NULL;
$arguments158['htmlEscape'] = NULL;
$arguments158['arguments'] = NULL;
$arguments158['extensionName'] = NULL;
$renderChildrenClosure159 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper160 = $self->getViewHelper('$viewHelper160', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper160->setArguments($arguments158);
$viewHelper160->setRenderingContext($renderingContext);
$viewHelper160->setRenderChildrenClosure($renderChildrenClosure159);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output151 .= $viewHelper160->initializeArgumentsAndRender();

$output151 .= '</td>
							<td>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments161 = array();
$arguments161['key'] = 'distribution.dependency.version';
$arguments161['id'] = NULL;
$arguments161['default'] = NULL;
$arguments161['htmlEscape'] = NULL;
$arguments161['arguments'] = NULL;
$arguments161['extensionName'] = NULL;
$renderChildrenClosure162 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper163 = $self->getViewHelper('$viewHelper163', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper163->setArguments($arguments161);
$viewHelper163->setRenderingContext($renderingContext);
$viewHelper163->setRenderChildrenClosure($renderChildrenClosure162);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output151 .= $viewHelper163->initializeArgumentsAndRender();

$output151 .= '</td>
						</tr>
					</thead>
					<tbody>
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper
$arguments164 = array();
$arguments164['each'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.dependencies', $renderingContext);
$arguments164['as'] = 'dependency';
$arguments164['key'] = '';
$arguments164['reverse'] = false;
$arguments164['iteration'] = NULL;
$renderChildrenClosure165 = function() use ($renderingContext, $self) {
$output166 = '';

$output166 .= '
							<tr>
								<td>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments167 = array();
$arguments167['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dependency.identifier', $renderingContext);
$arguments167['keepQuotes'] = false;
$arguments167['encoding'] = NULL;
$arguments167['doubleEncode'] = true;
$renderChildrenClosure168 = function() use ($renderingContext, $self) {
return NULL;
};
$value169 = ($arguments167['value'] !== NULL ? $arguments167['value'] : $renderChildrenClosure168());

$output166 .= (!is_string($value169) ? $value169 : htmlspecialchars($value169, ($arguments167['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments167['encoding'] !== NULL ? $arguments167['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments167['doubleEncode']));

$output166 .= '</td>
								<td>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments170 = array();
$arguments170['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dependency.type', $renderingContext);
$arguments170['keepQuotes'] = false;
$arguments170['encoding'] = NULL;
$arguments170['doubleEncode'] = true;
$renderChildrenClosure171 = function() use ($renderingContext, $self) {
return NULL;
};
$value172 = ($arguments170['value'] !== NULL ? $arguments170['value'] : $renderChildrenClosure171());

$output166 .= (!is_string($value172) ? $value172 : htmlspecialchars($value172, ($arguments170['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments170['encoding'] !== NULL ? $arguments170['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments170['doubleEncode']));

$output166 .= '</td>
								<td>
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments173 = array();
$arguments173['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dependency.lowestVersion', $renderingContext);
$arguments173['keepQuotes'] = false;
$arguments173['encoding'] = NULL;
$arguments173['doubleEncode'] = true;
$renderChildrenClosure174 = function() use ($renderingContext, $self) {
return NULL;
};
$value175 = ($arguments173['value'] !== NULL ? $arguments173['value'] : $renderChildrenClosure174());

$output166 .= (!is_string($value175) ? $value175 : htmlspecialchars($value175, ($arguments173['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments173['encoding'] !== NULL ? $arguments173['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments173['doubleEncode']));
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments176 = array();
// Rendering Boolean node
$arguments176['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dependency.highestVersion', $renderingContext));
$arguments176['then'] = NULL;
$arguments176['else'] = NULL;
$renderChildrenClosure177 = function() use ($renderingContext, $self) {
$output178 = '';

$output178 .= '-';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments179 = array();
$arguments179['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dependency.highestVersion', $renderingContext);
$arguments179['keepQuotes'] = false;
$arguments179['encoding'] = NULL;
$arguments179['doubleEncode'] = true;
$renderChildrenClosure180 = function() use ($renderingContext, $self) {
return NULL;
};
$value181 = ($arguments179['value'] !== NULL ? $arguments179['value'] : $renderChildrenClosure180());

$output178 .= (!is_string($value181) ? $value181 : htmlspecialchars($value181, ($arguments179['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments179['encoding'] !== NULL ? $arguments179['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments179['doubleEncode']));
return $output178;
};
$viewHelper182 = $self->getViewHelper('$viewHelper182', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper182->setArguments($arguments176);
$viewHelper182->setRenderingContext($renderingContext);
$viewHelper182->setRenderChildrenClosure($renderChildrenClosure177);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output166 .= $viewHelper182->initializeArgumentsAndRender();

$output166 .= '
								</td>
							</tr>
						';
return $output166;
};

$output151 .= TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments164, $renderChildrenClosure165, $renderingContext);

$output151 .= '
					</tbody>
				</table>
			';
return $output151;
};
$viewHelper183 = $self->getViewHelper('$viewHelper183', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper183->setArguments($arguments149);
$viewHelper183->setRenderingContext($renderingContext);
$viewHelper183->setRenderChildrenClosure($renderChildrenClosure150);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output10 .= $viewHelper183->initializeArgumentsAndRender();

$output10 .= '
		</div>
	</div>
';

return $output10;
}
/**
 * Main Render function
 */
public function render(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output184 = '';

$output184 .= '
';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\LayoutViewHelper
$arguments185 = array();
$arguments185['name'] = 'main';
$renderChildrenClosure186 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper187 = $self->getViewHelper('$viewHelper187', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\LayoutViewHelper');
$viewHelper187->setArguments($arguments185);
$viewHelper187->setRenderingContext($renderingContext);
$viewHelper187->setRenderChildrenClosure($renderChildrenClosure186);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\LayoutViewHelper

$output184 .= $viewHelper187->initializeArgumentsAndRender();

$output184 .= '

';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\SectionViewHelper
$arguments188 = array();
$arguments188['name'] = 'docheader-buttons';
$renderChildrenClosure189 = function() use ($renderingContext, $self) {
$output190 = '';

$output190 .= '
	';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments191 = array();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments192 = array();
$arguments192['controller'] = 'List';
$arguments192['action'] = 'distributions';
$arguments192['arguments'] = array (
);
$arguments192['extensionName'] = NULL;
$arguments192['pluginName'] = NULL;
$arguments192['pageUid'] = NULL;
$arguments192['pageType'] = 0;
$arguments192['noCache'] = false;
$arguments192['noCacheHash'] = false;
$arguments192['section'] = '';
$arguments192['format'] = '';
$arguments192['linkAccessRestrictedPages'] = false;
$arguments192['additionalParams'] = array (
);
$arguments192['absolute'] = false;
$arguments192['addQueryString'] = false;
$arguments192['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments192['addQueryStringMethod'] = NULL;
$renderChildrenClosure193 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper194 = $self->getViewHelper('$viewHelper194', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Uri\ActionViewHelper');
$viewHelper194->setArguments($arguments192);
$viewHelper194->setRenderingContext($renderingContext);
$viewHelper194->setRenderChildrenClosure($renderChildrenClosure193);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Uri\ActionViewHelper
$arguments191['uri'] = $viewHelper194->initializeArgumentsAndRender();
$arguments191['icon'] = 'actions-view-go-back';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments195 = array();
$arguments195['key'] = 'extConfTemplate.backToList';
$arguments195['id'] = NULL;
$arguments195['default'] = NULL;
$arguments195['htmlEscape'] = NULL;
$arguments195['arguments'] = NULL;
$arguments195['extensionName'] = NULL;
$renderChildrenClosure196 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper197 = $self->getViewHelper('$viewHelper197', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper197->setArguments($arguments195);
$viewHelper197->setRenderingContext($renderingContext);
$viewHelper197->setRenderChildrenClosure($renderChildrenClosure196);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments191['title'] = $viewHelper197->initializeArgumentsAndRender();
$arguments191['additionalAttributes'] = NULL;
$renderChildrenClosure198 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper199 = $self->getViewHelper('$viewHelper199', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper199->setArguments($arguments191);
$viewHelper199->setRenderingContext($renderingContext);
$viewHelper199->setRenderChildrenClosure($renderChildrenClosure198);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output190 .= $viewHelper199->initializeArgumentsAndRender();

$output190 .= '
';
return $output190;
};

$output184 .= '';

$output184 .= '

';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\SectionViewHelper
$arguments200 = array();
$arguments200['name'] = 'module-headline';
$renderChildrenClosure201 = function() use ($renderingContext, $self) {
return '
';
};

$output184 .= '';

$output184 .= '

';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\SectionViewHelper
$arguments202 = array();
$arguments202['name'] = 'Content';
$renderChildrenClosure203 = function() use ($renderingContext, $self) {
$output204 = '';

$output204 .= '
	';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\FlashMessagesViewHelper
$arguments205 = array();
$arguments205['class'] = 'distribution-detail-flashmessage';
$arguments205['renderMode'] = 'div';
$arguments205['additionalAttributes'] = NULL;
$arguments205['dir'] = NULL;
$arguments205['id'] = NULL;
$arguments205['lang'] = NULL;
$arguments205['style'] = NULL;
$arguments205['title'] = NULL;
$arguments205['accesskey'] = NULL;
$arguments205['tabindex'] = NULL;
$arguments205['onclick'] = NULL;
$renderChildrenClosure206 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper207 = $self->getViewHelper('$viewHelper207', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\FlashMessagesViewHelper');
$viewHelper207->setArguments($arguments205);
$viewHelper207->setRenderingContext($renderingContext);
$viewHelper207->setRenderChildrenClosure($renderChildrenClosure206);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\FlashMessagesViewHelper

$output204 .= $viewHelper207->initializeArgumentsAndRender();

$output204 .= '
	<div class="distribution-detail">
		<div class="distribution-detail-previewpane">
			';
// Rendering ViewHelper TYPO3\CMS\Extensionmanager\ViewHelpers\ImageViewHelper
$arguments208 = array();
$output209 = '';

$output209 .= 'EXT:';

$output209 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.extensionKey', $renderingContext);

$output209 .= '/Resources/Public/Images/DistributionWelcome.png';
$arguments208['src'] = $output209;
$arguments208['alt'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'distribution.title', $renderingContext);
$arguments208['class'] = 'distribution-detail-preview';
$arguments208['additionalAttributes'] = NULL;
$arguments208['width'] = NULL;
$arguments208['height'] = NULL;
$arguments208['minWidth'] = NULL;
$arguments208['minHeight'] = NULL;
$arguments208['maxWidth'] = NULL;
$arguments208['maxHeight'] = NULL;
$arguments208['fallbackImage'] = '';
$arguments208['dir'] = NULL;
$arguments208['id'] = NULL;
$arguments208['lang'] = NULL;
$arguments208['style'] = NULL;
$arguments208['title'] = NULL;
$arguments208['accesskey'] = NULL;
$arguments208['tabindex'] = NULL;
$arguments208['onclick'] = NULL;
$arguments208['ismap'] = NULL;
$arguments208['longdesc'] = NULL;
$arguments208['usemap'] = NULL;
$renderChildrenClosure210 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper211 = $self->getViewHelper('$viewHelper211', $renderingContext, 'TYPO3\CMS\Extensionmanager\ViewHelpers\ImageViewHelper');
$viewHelper211->setArguments($arguments208);
$viewHelper211->setRenderingContext($renderingContext);
$viewHelper211->setRenderChildrenClosure($renderChildrenClosure210);
// End of ViewHelper TYPO3\CMS\Extensionmanager\ViewHelpers\ImageViewHelper

$output204 .= $viewHelper211->initializeArgumentsAndRender();

$output204 .= '
		</div>
		<div class="distribution-detail-body">
			<div class="distribution-detail-header">
				<h1>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments212 = array();
$arguments212['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.title', $renderingContext);
$arguments212['keepQuotes'] = false;
$arguments212['encoding'] = NULL;
$arguments212['doubleEncode'] = true;
$renderChildrenClosure213 = function() use ($renderingContext, $self) {
return NULL;
};
$value214 = ($arguments212['value'] !== NULL ? $arguments212['value'] : $renderChildrenClosure213());

$output204 .= (!is_string($value214) ? $value214 : htmlspecialchars($value214, ($arguments212['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments212['encoding'] !== NULL ? $arguments212['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments212['doubleEncode']));

$output204 .= '</h1>
				<p>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments215 = array();
$arguments215['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.description', $renderingContext);
$arguments215['keepQuotes'] = false;
$arguments215['encoding'] = NULL;
$arguments215['doubleEncode'] = true;
$renderChildrenClosure216 = function() use ($renderingContext, $self) {
return NULL;
};
$value217 = ($arguments215['value'] !== NULL ? $arguments215['value'] : $renderChildrenClosure216());

$output204 .= (!is_string($value217) ? $value217 : htmlspecialchars($value217, ($arguments215['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments215['encoding'] !== NULL ? $arguments215['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments215['doubleEncode']));

$output204 .= '</p>
			</div>
			<ul class="list-unstyled">
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments218 = array();
// Rendering Boolean node
$arguments218['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'distributionActive', $renderingContext));
$arguments218['then'] = NULL;
$arguments218['else'] = NULL;
$renderChildrenClosure219 = function() use ($renderingContext, $self) {
$output220 = '';

$output220 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments221 = array();
$renderChildrenClosure222 = function() use ($renderingContext, $self) {
$output223 = '';

$output223 .= '
						<li>
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments224 = array();
$arguments224['action'] = 'installDistribution';
$arguments224['controller'] = 'Download';
// Rendering Array
$array225 = array();
$array225['extension'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension', $renderingContext);
$arguments224['arguments'] = $array225;
$arguments224['class'] = 't3-button t3-button-action-installdistribution';
$arguments224['additionalAttributes'] = NULL;
$arguments224['extensionName'] = NULL;
$arguments224['pluginName'] = NULL;
$arguments224['pageUid'] = NULL;
$arguments224['pageType'] = 0;
$arguments224['noCache'] = false;
$arguments224['noCacheHash'] = false;
$arguments224['section'] = '';
$arguments224['format'] = '';
$arguments224['linkAccessRestrictedPages'] = false;
$arguments224['additionalParams'] = array (
);
$arguments224['absolute'] = false;
$arguments224['addQueryString'] = false;
$arguments224['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments224['addQueryStringMethod'] = NULL;
$arguments224['dir'] = NULL;
$arguments224['id'] = NULL;
$arguments224['lang'] = NULL;
$arguments224['style'] = NULL;
$arguments224['title'] = NULL;
$arguments224['accesskey'] = NULL;
$arguments224['tabindex'] = NULL;
$arguments224['onclick'] = NULL;
$arguments224['name'] = NULL;
$arguments224['rel'] = NULL;
$arguments224['rev'] = NULL;
$arguments224['target'] = NULL;
$renderChildrenClosure226 = function() use ($renderingContext, $self) {
$output227 = '';

$output227 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments228 = array();
$arguments228['icon'] = 'actions-system-extension-import';
$arguments228['uri'] = '';
$arguments228['title'] = '';
$arguments228['additionalAttributes'] = NULL;
$renderChildrenClosure229 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper230 = $self->getViewHelper('$viewHelper230', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper230->setArguments($arguments228);
$viewHelper230->setRenderingContext($renderingContext);
$viewHelper230->setRenderChildrenClosure($renderChildrenClosure229);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output227 .= $viewHelper230->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments231 = array();
$arguments231['key'] = 'extensionList.installDistribution';
$arguments231['id'] = NULL;
$arguments231['default'] = NULL;
$arguments231['htmlEscape'] = NULL;
$arguments231['arguments'] = NULL;
$arguments231['extensionName'] = NULL;
$renderChildrenClosure232 = function() use ($renderingContext, $self) {
return 'Install';
};
$viewHelper233 = $self->getViewHelper('$viewHelper233', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper233->setArguments($arguments231);
$viewHelper233->setRenderingContext($renderingContext);
$viewHelper233->setRenderChildrenClosure($renderChildrenClosure232);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output227 .= $viewHelper233->initializeArgumentsAndRender();

$output227 .= '
							';
return $output227;
};
$viewHelper234 = $self->getViewHelper('$viewHelper234', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper234->setArguments($arguments224);
$viewHelper234->setRenderingContext($renderingContext);
$viewHelper234->setRenderChildrenClosure($renderChildrenClosure226);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Link\ActionViewHelper

$output223 .= $viewHelper234->initializeArgumentsAndRender();

$output223 .= '
						</li>
					';
return $output223;
};
$viewHelper235 = $self->getViewHelper('$viewHelper235', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper235->setArguments($arguments221);
$viewHelper235->setRenderingContext($renderingContext);
$viewHelper235->setRenderChildrenClosure($renderChildrenClosure222);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output220 .= $viewHelper235->initializeArgumentsAndRender();

$output220 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments236 = array();
$renderChildrenClosure237 = function() use ($renderingContext, $self) {
$output238 = '';

$output238 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments239 = array();
// Rendering Boolean node
$arguments239['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configurationLink', $renderingContext));
$arguments239['then'] = NULL;
$arguments239['else'] = NULL;
$renderChildrenClosure240 = function() use ($renderingContext, $self) {
$output241 = '';

$output241 .= '
							<li>
								<a href="';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments242 = array();
$arguments242['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configurationLink', $renderingContext);
$arguments242['keepQuotes'] = false;
$arguments242['encoding'] = NULL;
$arguments242['doubleEncode'] = true;
$renderChildrenClosure243 = function() use ($renderingContext, $self) {
return NULL;
};
$value244 = ($arguments242['value'] !== NULL ? $arguments242['value'] : $renderChildrenClosure243());

$output241 .= (!is_string($value244) ? $value244 : htmlspecialchars($value244, ($arguments242['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments242['encoding'] !== NULL ? $arguments242['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments242['doubleEncode']));

$output241 .= '" class="distribution-openViewModule t3-button" onclick="top.goToModule(\'web_ViewpageView\');">
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments245 = array();
$arguments245['icon'] = 'actions-system-extension-configure';
$arguments245['uri'] = '';
$arguments245['title'] = '';
$arguments245['additionalAttributes'] = NULL;
$renderChildrenClosure246 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper247 = $self->getViewHelper('$viewHelper247', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper247->setArguments($arguments245);
$viewHelper247->setRenderingContext($renderingContext);
$viewHelper247->setRenderChildrenClosure($renderChildrenClosure246);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output241 .= $viewHelper247->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments248 = array();
$arguments248['key'] = 'extensionList.configure';
$arguments248['id'] = NULL;
$arguments248['default'] = NULL;
$arguments248['htmlEscape'] = NULL;
$arguments248['arguments'] = NULL;
$arguments248['extensionName'] = NULL;
$renderChildrenClosure249 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper250 = $self->getViewHelper('$viewHelper250', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper250->setArguments($arguments248);
$viewHelper250->setRenderingContext($renderingContext);
$viewHelper250->setRenderChildrenClosure($renderChildrenClosure249);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output241 .= $viewHelper250->initializeArgumentsAndRender();

$output241 .= '
								</a>
							</li>
						';
return $output241;
};
$viewHelper251 = $self->getViewHelper('$viewHelper251', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper251->setArguments($arguments239);
$viewHelper251->setRenderingContext($renderingContext);
$viewHelper251->setRenderChildrenClosure($renderChildrenClosure240);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output238 .= $viewHelper251->initializeArgumentsAndRender();

$output238 .= '
						<li>
							<button class="distribution-openViewModule t3-button" onclick="top.goToModule(\'web_ViewpageView\');">
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments252 = array();
$arguments252['icon'] = 'actions-document-view';
$arguments252['uri'] = '';
$arguments252['title'] = '';
$arguments252['additionalAttributes'] = NULL;
$renderChildrenClosure253 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper254 = $self->getViewHelper('$viewHelper254', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper254->setArguments($arguments252);
$viewHelper254->setRenderingContext($renderingContext);
$viewHelper254->setRenderChildrenClosure($renderChildrenClosure253);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output238 .= $viewHelper254->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments255 = array();
$arguments255['key'] = 'distribution.welcome.openViewModule';
$arguments255['id'] = NULL;
$arguments255['default'] = NULL;
$arguments255['htmlEscape'] = NULL;
$arguments255['arguments'] = NULL;
$arguments255['extensionName'] = NULL;
$renderChildrenClosure256 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper257 = $self->getViewHelper('$viewHelper257', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper257->setArguments($arguments255);
$viewHelper257->setRenderingContext($renderingContext);
$viewHelper257->setRenderChildrenClosure($renderChildrenClosure256);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output238 .= $viewHelper257->initializeArgumentsAndRender();

$output238 .= '
							</button>
						</li>
						<li>
							<button class="distribution-openPageModule t3-button" onclick="top.goToModule(\'web_page\');">
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments258 = array();
$arguments258['icon'] = 'actions-document-open';
$arguments258['uri'] = '';
$arguments258['title'] = '';
$arguments258['additionalAttributes'] = NULL;
$renderChildrenClosure259 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper260 = $self->getViewHelper('$viewHelper260', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper260->setArguments($arguments258);
$viewHelper260->setRenderingContext($renderingContext);
$viewHelper260->setRenderChildrenClosure($renderChildrenClosure259);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output238 .= $viewHelper260->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments261 = array();
$arguments261['key'] = 'distribution.welcome.openPageModule';
$arguments261['id'] = NULL;
$arguments261['default'] = NULL;
$arguments261['htmlEscape'] = NULL;
$arguments261['arguments'] = NULL;
$arguments261['extensionName'] = NULL;
$renderChildrenClosure262 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper263 = $self->getViewHelper('$viewHelper263', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper263->setArguments($arguments261);
$viewHelper263->setRenderingContext($renderingContext);
$viewHelper263->setRenderChildrenClosure($renderChildrenClosure262);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output238 .= $viewHelper263->initializeArgumentsAndRender();

$output238 .= '
							</button>
						</li>
					';
return $output238;
};
$viewHelper264 = $self->getViewHelper('$viewHelper264', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper264->setArguments($arguments236);
$viewHelper264->setRenderingContext($renderingContext);
$viewHelper264->setRenderChildrenClosure($renderChildrenClosure237);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output220 .= $viewHelper264->initializeArgumentsAndRender();

$output220 .= '
				';
return $output220;
};
$arguments218['__elseClosure'] = function() use ($renderingContext, $self) {
$output265 = '';

$output265 .= '
						<li>
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Link\ActionViewHelper
$arguments266 = array();
$arguments266['action'] = 'installDistribution';
$arguments266['controller'] = 'Download';
// Rendering Array
$array267 = array();
$array267['extension'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension', $renderingContext);
$arguments266['arguments'] = $array267;
$arguments266['class'] = 't3-button t3-button-action-installdistribution';
$arguments266['additionalAttributes'] = NULL;
$arguments266['extensionName'] = NULL;
$arguments266['pluginName'] = NULL;
$arguments266['pageUid'] = NULL;
$arguments266['pageType'] = 0;
$arguments266['noCache'] = false;
$arguments266['noCacheHash'] = false;
$arguments266['section'] = '';
$arguments266['format'] = '';
$arguments266['linkAccessRestrictedPages'] = false;
$arguments266['additionalParams'] = array (
);
$arguments266['absolute'] = false;
$arguments266['addQueryString'] = false;
$arguments266['argumentsToBeExcludedFromQueryString'] = array (
);
$arguments266['addQueryStringMethod'] = NULL;
$arguments266['dir'] = NULL;
$arguments266['id'] = NULL;
$arguments266['lang'] = NULL;
$arguments266['style'] = NULL;
$arguments266['title'] = NULL;
$arguments266['accesskey'] = NULL;
$arguments266['tabindex'] = NULL;
$arguments266['onclick'] = NULL;
$arguments266['name'] = NULL;
$arguments266['rel'] = NULL;
$arguments266['rev'] = NULL;
$arguments266['target'] = NULL;
$renderChildrenClosure268 = function() use ($renderingContext, $self) {
$output269 = '';

$output269 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments270 = array();
$arguments270['icon'] = 'actions-system-extension-import';
$arguments270['uri'] = '';
$arguments270['title'] = '';
$arguments270['additionalAttributes'] = NULL;
$renderChildrenClosure271 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper272 = $self->getViewHelper('$viewHelper272', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper272->setArguments($arguments270);
$viewHelper272->setRenderingContext($renderingContext);
$viewHelper272->setRenderChildrenClosure($renderChildrenClosure271);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output269 .= $viewHelper272->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments273 = array();
$arguments273['key'] = 'extensionList.installDistribution';
$arguments273['id'] = NULL;
$arguments273['default'] = NULL;
$arguments273['htmlEscape'] = NULL;
$arguments273['arguments'] = NULL;
$arguments273['extensionName'] = NULL;
$renderChildrenClosure274 = function() use ($renderingContext, $self) {
return 'Install';
};
$viewHelper275 = $self->getViewHelper('$viewHelper275', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper275->setArguments($arguments273);
$viewHelper275->setRenderingContext($renderingContext);
$viewHelper275->setRenderChildrenClosure($renderChildrenClosure274);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output269 .= $viewHelper275->initializeArgumentsAndRender();

$output269 .= '
							';
return $output269;
};
$viewHelper276 = $self->getViewHelper('$viewHelper276', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Link\ActionViewHelper');
$viewHelper276->setArguments($arguments266);
$viewHelper276->setRenderingContext($renderingContext);
$viewHelper276->setRenderChildrenClosure($renderChildrenClosure268);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Link\ActionViewHelper

$output265 .= $viewHelper276->initializeArgumentsAndRender();

$output265 .= '
						</li>
					';
return $output265;
};
$arguments218['__thenClosure'] = function() use ($renderingContext, $self) {
$output277 = '';

$output277 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments278 = array();
// Rendering Boolean node
$arguments278['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configurationLink', $renderingContext));
$arguments278['then'] = NULL;
$arguments278['else'] = NULL;
$renderChildrenClosure279 = function() use ($renderingContext, $self) {
$output280 = '';

$output280 .= '
							<li>
								<a href="';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments281 = array();
$arguments281['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configurationLink', $renderingContext);
$arguments281['keepQuotes'] = false;
$arguments281['encoding'] = NULL;
$arguments281['doubleEncode'] = true;
$renderChildrenClosure282 = function() use ($renderingContext, $self) {
return NULL;
};
$value283 = ($arguments281['value'] !== NULL ? $arguments281['value'] : $renderChildrenClosure282());

$output280 .= (!is_string($value283) ? $value283 : htmlspecialchars($value283, ($arguments281['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments281['encoding'] !== NULL ? $arguments281['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments281['doubleEncode']));

$output280 .= '" class="distribution-openViewModule t3-button" onclick="top.goToModule(\'web_ViewpageView\');">
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments284 = array();
$arguments284['icon'] = 'actions-system-extension-configure';
$arguments284['uri'] = '';
$arguments284['title'] = '';
$arguments284['additionalAttributes'] = NULL;
$renderChildrenClosure285 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper286 = $self->getViewHelper('$viewHelper286', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper286->setArguments($arguments284);
$viewHelper286->setRenderingContext($renderingContext);
$viewHelper286->setRenderChildrenClosure($renderChildrenClosure285);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output280 .= $viewHelper286->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments287 = array();
$arguments287['key'] = 'extensionList.configure';
$arguments287['id'] = NULL;
$arguments287['default'] = NULL;
$arguments287['htmlEscape'] = NULL;
$arguments287['arguments'] = NULL;
$arguments287['extensionName'] = NULL;
$renderChildrenClosure288 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper289 = $self->getViewHelper('$viewHelper289', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper289->setArguments($arguments287);
$viewHelper289->setRenderingContext($renderingContext);
$viewHelper289->setRenderChildrenClosure($renderChildrenClosure288);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output280 .= $viewHelper289->initializeArgumentsAndRender();

$output280 .= '
								</a>
							</li>
						';
return $output280;
};
$viewHelper290 = $self->getViewHelper('$viewHelper290', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper290->setArguments($arguments278);
$viewHelper290->setRenderingContext($renderingContext);
$viewHelper290->setRenderChildrenClosure($renderChildrenClosure279);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output277 .= $viewHelper290->initializeArgumentsAndRender();

$output277 .= '
						<li>
							<button class="distribution-openViewModule t3-button" onclick="top.goToModule(\'web_ViewpageView\');">
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments291 = array();
$arguments291['icon'] = 'actions-document-view';
$arguments291['uri'] = '';
$arguments291['title'] = '';
$arguments291['additionalAttributes'] = NULL;
$renderChildrenClosure292 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper293 = $self->getViewHelper('$viewHelper293', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper293->setArguments($arguments291);
$viewHelper293->setRenderingContext($renderingContext);
$viewHelper293->setRenderChildrenClosure($renderChildrenClosure292);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output277 .= $viewHelper293->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments294 = array();
$arguments294['key'] = 'distribution.welcome.openViewModule';
$arguments294['id'] = NULL;
$arguments294['default'] = NULL;
$arguments294['htmlEscape'] = NULL;
$arguments294['arguments'] = NULL;
$arguments294['extensionName'] = NULL;
$renderChildrenClosure295 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper296 = $self->getViewHelper('$viewHelper296', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper296->setArguments($arguments294);
$viewHelper296->setRenderingContext($renderingContext);
$viewHelper296->setRenderChildrenClosure($renderChildrenClosure295);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output277 .= $viewHelper296->initializeArgumentsAndRender();

$output277 .= '
							</button>
						</li>
						<li>
							<button class="distribution-openPageModule t3-button" onclick="top.goToModule(\'web_page\');">
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper
$arguments297 = array();
$arguments297['icon'] = 'actions-document-open';
$arguments297['uri'] = '';
$arguments297['title'] = '';
$arguments297['additionalAttributes'] = NULL;
$renderChildrenClosure298 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper299 = $self->getViewHelper('$viewHelper299', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper');
$viewHelper299->setArguments($arguments297);
$viewHelper299->setRenderingContext($renderingContext);
$viewHelper299->setRenderChildrenClosure($renderChildrenClosure298);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Be\Buttons\IconViewHelper

$output277 .= $viewHelper299->initializeArgumentsAndRender();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments300 = array();
$arguments300['key'] = 'distribution.welcome.openPageModule';
$arguments300['id'] = NULL;
$arguments300['default'] = NULL;
$arguments300['htmlEscape'] = NULL;
$arguments300['arguments'] = NULL;
$arguments300['extensionName'] = NULL;
$renderChildrenClosure301 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper302 = $self->getViewHelper('$viewHelper302', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper302->setArguments($arguments300);
$viewHelper302->setRenderingContext($renderingContext);
$viewHelper302->setRenderChildrenClosure($renderChildrenClosure301);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output277 .= $viewHelper302->initializeArgumentsAndRender();

$output277 .= '
							</button>
						</li>
					';
return $output277;
};
$viewHelper303 = $self->getViewHelper('$viewHelper303', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper303->setArguments($arguments218);
$viewHelper303->setRenderingContext($renderingContext);
$viewHelper303->setRenderChildrenClosure($renderChildrenClosure219);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output204 .= $viewHelper303->initializeArgumentsAndRender();

$output204 .= '
			</ul>
			<dl class="description-horizontal description-horizontal-wide distribution-detail-summary">
				<dt>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments304 = array();
$arguments304['key'] = 'extensionList.distribution.title';
$arguments304['id'] = NULL;
$arguments304['default'] = NULL;
$arguments304['htmlEscape'] = NULL;
$arguments304['arguments'] = NULL;
$arguments304['extensionName'] = NULL;
$renderChildrenClosure305 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper306 = $self->getViewHelper('$viewHelper306', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper306->setArguments($arguments304);
$viewHelper306->setRenderingContext($renderingContext);
$viewHelper306->setRenderChildrenClosure($renderChildrenClosure305);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output204 .= $viewHelper306->initializeArgumentsAndRender();

$output204 .= '</dt>
				<dd>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments307 = array();
$arguments307['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.title', $renderingContext);
$arguments307['keepQuotes'] = false;
$arguments307['encoding'] = NULL;
$arguments307['doubleEncode'] = true;
$renderChildrenClosure308 = function() use ($renderingContext, $self) {
return NULL;
};
$value309 = ($arguments307['value'] !== NULL ? $arguments307['value'] : $renderChildrenClosure308());

$output204 .= (!is_string($value309) ? $value309 : htmlspecialchars($value309, ($arguments307['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments307['encoding'] !== NULL ? $arguments307['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments307['doubleEncode']));

$output204 .= '</dd>
				<dt>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments310 = array();
$arguments310['key'] = 'extensionList.distribution.key';
$arguments310['id'] = NULL;
$arguments310['default'] = NULL;
$arguments310['htmlEscape'] = NULL;
$arguments310['arguments'] = NULL;
$arguments310['extensionName'] = NULL;
$renderChildrenClosure311 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper312 = $self->getViewHelper('$viewHelper312', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper312->setArguments($arguments310);
$viewHelper312->setRenderingContext($renderingContext);
$viewHelper312->setRenderChildrenClosure($renderChildrenClosure311);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output204 .= $viewHelper312->initializeArgumentsAndRender();

$output204 .= '</dt>
				<dd>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments313 = array();
$arguments313['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.extensionKey', $renderingContext);
$arguments313['keepQuotes'] = false;
$arguments313['encoding'] = NULL;
$arguments313['doubleEncode'] = true;
$renderChildrenClosure314 = function() use ($renderingContext, $self) {
return NULL;
};
$value315 = ($arguments313['value'] !== NULL ? $arguments313['value'] : $renderChildrenClosure314());

$output204 .= (!is_string($value315) ? $value315 : htmlspecialchars($value315, ($arguments313['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments313['encoding'] !== NULL ? $arguments313['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments313['doubleEncode']));

$output204 .= '</dd>
				<dt>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments316 = array();
$arguments316['key'] = 'extensionList.distribution.version';
$arguments316['id'] = NULL;
$arguments316['default'] = NULL;
$arguments316['htmlEscape'] = NULL;
$arguments316['arguments'] = NULL;
$arguments316['extensionName'] = NULL;
$renderChildrenClosure317 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper318 = $self->getViewHelper('$viewHelper318', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper318->setArguments($arguments316);
$viewHelper318->setRenderingContext($renderingContext);
$viewHelper318->setRenderChildrenClosure($renderChildrenClosure317);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output204 .= $viewHelper318->initializeArgumentsAndRender();

$output204 .= '</dt>
				<dd>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments319 = array();
$arguments319['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.version', $renderingContext);
$arguments319['keepQuotes'] = false;
$arguments319['encoding'] = NULL;
$arguments319['doubleEncode'] = true;
$renderChildrenClosure320 = function() use ($renderingContext, $self) {
return NULL;
};
$value321 = ($arguments319['value'] !== NULL ? $arguments319['value'] : $renderChildrenClosure320());

$output204 .= (!is_string($value321) ? $value321 : htmlspecialchars($value321, ($arguments319['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments319['encoding'] !== NULL ? $arguments319['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments319['doubleEncode']));

$output204 .= ' (';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\DateViewHelper
$arguments322 = array();
$arguments322['format'] = 'd.m.Y';
$arguments322['date'] = NULL;
$renderChildrenClosure323 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.lastUpdated', $renderingContext);
};
$viewHelper324 = $self->getViewHelper('$viewHelper324', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\DateViewHelper');
$viewHelper324->setArguments($arguments322);
$viewHelper324->setRenderingContext($renderingContext);
$viewHelper324->setRenderChildrenClosure($renderChildrenClosure323);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\DateViewHelper

$output204 .= $viewHelper324->initializeArgumentsAndRender();

$output204 .= ')<br><span class="';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments325 = array();
$arguments325['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.stateString', $renderingContext);
$arguments325['keepQuotes'] = false;
$arguments325['encoding'] = NULL;
$arguments325['doubleEncode'] = true;
$renderChildrenClosure326 = function() use ($renderingContext, $self) {
return NULL;
};
$value327 = ($arguments325['value'] !== NULL ? $arguments325['value'] : $renderChildrenClosure326());

$output204 .= (!is_string($value327) ? $value327 : htmlspecialchars($value327, ($arguments325['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments325['encoding'] !== NULL ? $arguments325['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments325['doubleEncode']));

$output204 .= '">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments328 = array();
$arguments328['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.stateString', $renderingContext);
$arguments328['keepQuotes'] = false;
$arguments328['encoding'] = NULL;
$arguments328['doubleEncode'] = true;
$renderChildrenClosure329 = function() use ($renderingContext, $self) {
return NULL;
};
$value330 = ($arguments328['value'] !== NULL ? $arguments328['value'] : $renderChildrenClosure329());

$output204 .= (!is_string($value330) ? $value330 : htmlspecialchars($value330, ($arguments328['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments328['encoding'] !== NULL ? $arguments328['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments328['doubleEncode']));

$output204 .= '</span></dd>
				<dt>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments331 = array();
$arguments331['key'] = 'extensionList.distribution.author';
$arguments331['id'] = NULL;
$arguments331['default'] = NULL;
$arguments331['htmlEscape'] = NULL;
$arguments331['arguments'] = NULL;
$arguments331['extensionName'] = NULL;
$renderChildrenClosure332 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper333 = $self->getViewHelper('$viewHelper333', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper333->setArguments($arguments331);
$viewHelper333->setRenderingContext($renderingContext);
$viewHelper333->setRenderChildrenClosure($renderChildrenClosure332);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output204 .= $viewHelper333->initializeArgumentsAndRender();

$output204 .= '</dt>
				<dd>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments334 = array();
$arguments334['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.authorName', $renderingContext);
$arguments334['keepQuotes'] = false;
$arguments334['encoding'] = NULL;
$arguments334['doubleEncode'] = true;
$renderChildrenClosure335 = function() use ($renderingContext, $self) {
return NULL;
};
$value336 = ($arguments334['value'] !== NULL ? $arguments334['value'] : $renderChildrenClosure335());

$output204 .= (!is_string($value336) ? $value336 : htmlspecialchars($value336, ($arguments334['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments334['encoding'] !== NULL ? $arguments334['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments334['doubleEncode']));

$output204 .= '</dd>
				<dt>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments337 = array();
$arguments337['key'] = 'extensionList.distribution.downloads';
$arguments337['id'] = NULL;
$arguments337['default'] = NULL;
$arguments337['htmlEscape'] = NULL;
$arguments337['arguments'] = NULL;
$arguments337['extensionName'] = NULL;
$renderChildrenClosure338 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper339 = $self->getViewHelper('$viewHelper339', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper339->setArguments($arguments337);
$viewHelper339->setRenderingContext($renderingContext);
$viewHelper339->setRenderChildrenClosure($renderChildrenClosure338);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output204 .= $viewHelper339->initializeArgumentsAndRender();

$output204 .= '</dt>
				<dd>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments340 = array();
$arguments340['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.alldownloadcounter', $renderingContext);
$arguments340['keepQuotes'] = false;
$arguments340['encoding'] = NULL;
$arguments340['doubleEncode'] = true;
$renderChildrenClosure341 = function() use ($renderingContext, $self) {
return NULL;
};
$value342 = ($arguments340['value'] !== NULL ? $arguments340['value'] : $renderChildrenClosure341());

$output204 .= (!is_string($value342) ? $value342 : htmlspecialchars($value342, ($arguments340['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments340['encoding'] !== NULL ? $arguments340['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments340['doubleEncode']));

$output204 .= '</dd>
			</dl>
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments343 = array();
// Rendering Boolean node
$arguments343['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.dependencies', $renderingContext));
$arguments343['then'] = NULL;
$arguments343['else'] = NULL;
$renderChildrenClosure344 = function() use ($renderingContext, $self) {
$output345 = '';

$output345 .= '
				<h2>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments346 = array();
$arguments346['key'] = 'distribution.dependency.headline';
$arguments346['id'] = NULL;
$arguments346['default'] = NULL;
$arguments346['htmlEscape'] = NULL;
$arguments346['arguments'] = NULL;
$arguments346['extensionName'] = NULL;
$renderChildrenClosure347 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper348 = $self->getViewHelper('$viewHelper348', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper348->setArguments($arguments346);
$viewHelper348->setRenderingContext($renderingContext);
$viewHelper348->setRenderChildrenClosure($renderChildrenClosure347);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output345 .= $viewHelper348->initializeArgumentsAndRender();

$output345 .= '</h2>
				<table class="t3-table">
					<thead>
						<tr class="t3-row-header">
							<td>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments349 = array();
$arguments349['key'] = 'distribution.dependency.identifier';
$arguments349['id'] = NULL;
$arguments349['default'] = NULL;
$arguments349['htmlEscape'] = NULL;
$arguments349['arguments'] = NULL;
$arguments349['extensionName'] = NULL;
$renderChildrenClosure350 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper351 = $self->getViewHelper('$viewHelper351', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper351->setArguments($arguments349);
$viewHelper351->setRenderingContext($renderingContext);
$viewHelper351->setRenderChildrenClosure($renderChildrenClosure350);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output345 .= $viewHelper351->initializeArgumentsAndRender();

$output345 .= '</td>
							<td>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments352 = array();
$arguments352['key'] = 'distribution.dependency.type';
$arguments352['id'] = NULL;
$arguments352['default'] = NULL;
$arguments352['htmlEscape'] = NULL;
$arguments352['arguments'] = NULL;
$arguments352['extensionName'] = NULL;
$renderChildrenClosure353 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper354 = $self->getViewHelper('$viewHelper354', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper354->setArguments($arguments352);
$viewHelper354->setRenderingContext($renderingContext);
$viewHelper354->setRenderChildrenClosure($renderChildrenClosure353);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output345 .= $viewHelper354->initializeArgumentsAndRender();

$output345 .= '</td>
							<td>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments355 = array();
$arguments355['key'] = 'distribution.dependency.version';
$arguments355['id'] = NULL;
$arguments355['default'] = NULL;
$arguments355['htmlEscape'] = NULL;
$arguments355['arguments'] = NULL;
$arguments355['extensionName'] = NULL;
$renderChildrenClosure356 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper357 = $self->getViewHelper('$viewHelper357', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper357->setArguments($arguments355);
$viewHelper357->setRenderingContext($renderingContext);
$viewHelper357->setRenderChildrenClosure($renderChildrenClosure356);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output345 .= $viewHelper357->initializeArgumentsAndRender();

$output345 .= '</td>
						</tr>
					</thead>
					<tbody>
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper
$arguments358 = array();
$arguments358['each'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'extension.dependencies', $renderingContext);
$arguments358['as'] = 'dependency';
$arguments358['key'] = '';
$arguments358['reverse'] = false;
$arguments358['iteration'] = NULL;
$renderChildrenClosure359 = function() use ($renderingContext, $self) {
$output360 = '';

$output360 .= '
							<tr>
								<td>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments361 = array();
$arguments361['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dependency.identifier', $renderingContext);
$arguments361['keepQuotes'] = false;
$arguments361['encoding'] = NULL;
$arguments361['doubleEncode'] = true;
$renderChildrenClosure362 = function() use ($renderingContext, $self) {
return NULL;
};
$value363 = ($arguments361['value'] !== NULL ? $arguments361['value'] : $renderChildrenClosure362());

$output360 .= (!is_string($value363) ? $value363 : htmlspecialchars($value363, ($arguments361['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments361['encoding'] !== NULL ? $arguments361['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments361['doubleEncode']));

$output360 .= '</td>
								<td>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments364 = array();
$arguments364['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dependency.type', $renderingContext);
$arguments364['keepQuotes'] = false;
$arguments364['encoding'] = NULL;
$arguments364['doubleEncode'] = true;
$renderChildrenClosure365 = function() use ($renderingContext, $self) {
return NULL;
};
$value366 = ($arguments364['value'] !== NULL ? $arguments364['value'] : $renderChildrenClosure365());

$output360 .= (!is_string($value366) ? $value366 : htmlspecialchars($value366, ($arguments364['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments364['encoding'] !== NULL ? $arguments364['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments364['doubleEncode']));

$output360 .= '</td>
								<td>
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments367 = array();
$arguments367['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dependency.lowestVersion', $renderingContext);
$arguments367['keepQuotes'] = false;
$arguments367['encoding'] = NULL;
$arguments367['doubleEncode'] = true;
$renderChildrenClosure368 = function() use ($renderingContext, $self) {
return NULL;
};
$value369 = ($arguments367['value'] !== NULL ? $arguments367['value'] : $renderChildrenClosure368());

$output360 .= (!is_string($value369) ? $value369 : htmlspecialchars($value369, ($arguments367['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments367['encoding'] !== NULL ? $arguments367['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments367['doubleEncode']));
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments370 = array();
// Rendering Boolean node
$arguments370['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dependency.highestVersion', $renderingContext));
$arguments370['then'] = NULL;
$arguments370['else'] = NULL;
$renderChildrenClosure371 = function() use ($renderingContext, $self) {
$output372 = '';

$output372 .= '-';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments373 = array();
$arguments373['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dependency.highestVersion', $renderingContext);
$arguments373['keepQuotes'] = false;
$arguments373['encoding'] = NULL;
$arguments373['doubleEncode'] = true;
$renderChildrenClosure374 = function() use ($renderingContext, $self) {
return NULL;
};
$value375 = ($arguments373['value'] !== NULL ? $arguments373['value'] : $renderChildrenClosure374());

$output372 .= (!is_string($value375) ? $value375 : htmlspecialchars($value375, ($arguments373['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments373['encoding'] !== NULL ? $arguments373['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments373['doubleEncode']));
return $output372;
};
$viewHelper376 = $self->getViewHelper('$viewHelper376', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper376->setArguments($arguments370);
$viewHelper376->setRenderingContext($renderingContext);
$viewHelper376->setRenderChildrenClosure($renderChildrenClosure371);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output360 .= $viewHelper376->initializeArgumentsAndRender();

$output360 .= '
								</td>
							</tr>
						';
return $output360;
};

$output345 .= TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments358, $renderChildrenClosure359, $renderingContext);

$output345 .= '
					</tbody>
				</table>
			';
return $output345;
};
$viewHelper377 = $self->getViewHelper('$viewHelper377', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper377->setArguments($arguments343);
$viewHelper377->setRenderingContext($renderingContext);
$viewHelper377->setRenderChildrenClosure($renderChildrenClosure344);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output204 .= $viewHelper377->initializeArgumentsAndRender();

$output204 .= '
		</div>
	</div>
';
return $output204;
};

$output184 .= '';

return $output184;
}


}
#1411202403    112837    