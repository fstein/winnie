..  Editor configuration
	...................................................
	* utf-8 with BOM as encoding
	* tab indent with 4 characters for code snippet.
	* optional: soft carriage return preferred.

.. Includes roles, substitutions, ...
.. include:: _IncludedDirectives.rst

=================
Extension Name
=================

:Extension name: Bootstrap Kickstart Package
:Extension key: bsdist
:Version: 1.0.1
:Description: manual covering TYPO3 extension "bsdist"
:Language: en
:Author: Pascal Mayer
:Creation: 2014-08-17
:Generation: 17:00
:Licence: Open Content License available from `www.opencontent.org/opl.shtml <http://www.opencontent.org/opl.shtml>`_

The content of this document is related to TYPO3, a GNU/GPL CMS/Framework available from `www.typo3.org
<http://www.typo3.org/>`_


What does it do?
=================

See http://bsdist.ch for more information.
